<?php
namespace application\models;
class Manager
{
    private $return_array = array();
    private $create_array = array();
    private $save_array = array();

    public function getListData()
    {
        return $this->$return_array;
    }

    public function setListData($cool_array = array())
    {
        $con = new \MongoClient();
        $collection = $con->test->persons;
        $filter = $cool_array;
        $list = $collection->find($filter);
        $i = 0;
        while ($document = $list->getNext()) {
            $return_array[i] = array("name" => $document["name"], "pass" => $document["pass"], "last_name" => $document["last_name"]);
            $i++;
        }
        //$i = 0;
        $con->close();
        $this->return_array = $return_array;
    }

    public function get_one()
    {
    }

    public function getCreate()
    {
        return $this->$create_array;
    }

    public function setCreate($crate_array = array())
    {
        $con = new \MongoClient();
        $collection = $con->test->persons;
        $collection->insert($crate_array);
        $con->close();

    }

    public function getSaveComment()
    {
        return $this->$save_array;
    }

    public function setSaveComment($save_array = array())
    {
        $con = new \MongoClient();
        $collection = $con->test->comments;
        $collection->insert($save_array);
        $con->close();
    }
}

?>